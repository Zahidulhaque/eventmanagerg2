package ee.sda.training.eventmanager.service;

import ee.sda.training.eventmanager.model.Event;
import ee.sda.training.eventmanager.model.EventTimeVenue;
import ee.sda.training.eventmanager.model.enumeration.Language;
import ee.sda.training.eventmanager.repository.AboutRepository;
import ee.sda.training.eventmanager.repository.EventRepository;
import ee.sda.training.eventmanager.repository.EventTimeVenueRepository;
import ee.sda.training.eventmanager.repository.NoteRepository;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class EventServiceImpl implements EventService {
    AboutRepository aboutRepository;
    NoteRepository noteRepository;
    EventRepository eventRepository;
    EventTimeVenueRepository eventTimeVenueRepository;
    public EventServiceImpl(
            AboutRepository aboutRepository,
            NoteRepository noteRepository,
            EventRepository eventRepository,
            EventTimeVenueRepository eventTimeVenueRepository
            ) {
        this.aboutRepository = aboutRepository;
        this.noteRepository = noteRepository;
        this.eventRepository = eventRepository;
        this.eventTimeVenueRepository = eventTimeVenueRepository;
    }


    public Optional<Event> findByIdAndLanguage(Long id, String language) {
        Optional<Event> event = eventRepository.findById(id);
        Language ln = language == null ?  Language.et : Language.valueOf(language);
        event.ifPresent(value -> value.setAbouts(aboutRepository.findAllByEventIdAndLanguage(id, ln)));
        event.ifPresent(value -> value.getEventTimeVenues().forEach(
                occurrence -> {
                    occurrence
                            .getVenue().setAbouts(aboutRepository.findAllByVenueIdAndLanguage(
                            occurrence.getVenue().getId(), ln));
                    occurrence.setNotes(noteRepository.findAllByEventTimeVenue_IdAndLanguage(occurrence.getId(), ln));
                }
        ));

        return event;
    }

    public List<EventTimeVenue> findAllUpcomingByLanguage(String language, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate localDate) {

        List<EventTimeVenue> eventsByTimeVenue = eventTimeVenueRepository.findAllByStartDateGreaterThanEqualOrderByStartDateAscStartTimeAsc(localDate);
       Language ln = language == null ? Language.et : Language.valueOf(language);
        eventsByTimeVenue.forEach(eventTimeVenue -> {
            eventTimeVenue.getEvent()
                    .setAbouts(aboutRepository.findAllByEventIdAndLanguage(
                            eventTimeVenue.getEvent().getId(), ln));
            eventTimeVenue.getVenue()
                    .setAbouts(aboutRepository
                            .findAllByVenueIdAndLanguage(eventTimeVenue.getVenue().getId(), ln));

        });
        return eventsByTimeVenue;
    }
}

