package ee.sda.training.eventmanager.service;

import java.util.ArrayList;
import java.util.List;

public class Trial {
    public static void main(String[] args) {
        List<Integer> nums = new ArrayList<>();
        nums.add(2);
        nums.add(3);
        nums.add(4);
        nums.add(5);
        Number res = Helper.multiply(nums.toArray());
        System.out.println(res);
    }
}
