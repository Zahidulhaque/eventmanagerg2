package ee.sda.training.eventmanager.service;

import ee.sda.training.eventmanager.model.Event;
import ee.sda.training.eventmanager.model.EventTimeVenue;
import ee.sda.training.eventmanager.model.enumeration.Language;
import ee.sda.training.eventmanager.repository.EventRepository;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
public interface EventService {
    Optional<Event> findByIdAndLanguage(Long id, String language);

    List<EventTimeVenue> findAllUpcomingByLanguage(String language, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate now);
}
