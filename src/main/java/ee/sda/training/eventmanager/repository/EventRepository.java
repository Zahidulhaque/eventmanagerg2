package ee.sda.training.eventmanager.repository;

import ee.sda.training.eventmanager.model.Event;
import ee.sda.training.eventmanager.model.enumeration.Language;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface EventRepository extends JpaRepository<Event, Long>, JpaSpecificationExecutor<Event> {
}
