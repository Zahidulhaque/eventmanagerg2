package ee.sda.training.eventmanager.repository;

import ee.sda.training.eventmanager.model.JhiDateTimeWrapper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface JhiDateTimeWrapperRepository extends JpaRepository<JhiDateTimeWrapper, Long>, JpaSpecificationExecutor<JhiDateTimeWrapper> {

}
