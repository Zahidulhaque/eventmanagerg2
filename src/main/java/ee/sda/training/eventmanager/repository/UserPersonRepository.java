package ee.sda.training.eventmanager.repository;

import ee.sda.training.eventmanager.model.UserPerson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserPersonRepository extends JpaRepository<UserPerson, Long>, JpaSpecificationExecutor<UserPerson> {

}
