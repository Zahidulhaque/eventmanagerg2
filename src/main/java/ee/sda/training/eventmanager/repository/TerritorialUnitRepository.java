package ee.sda.training.eventmanager.repository;

import ee.sda.training.eventmanager.model.TerritorialUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TerritorialUnitRepository extends JpaRepository<TerritorialUnit, Long>, JpaSpecificationExecutor<TerritorialUnit> {

}
