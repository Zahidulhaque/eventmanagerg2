package ee.sda.training.eventmanager.repository;

import ee.sda.training.eventmanager.model.EventOrganizerAndContact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface EventOrganizerAndContactRepository extends JpaRepository<EventOrganizerAndContact, Long>, JpaSpecificationExecutor<EventOrganizerAndContact> {

}
