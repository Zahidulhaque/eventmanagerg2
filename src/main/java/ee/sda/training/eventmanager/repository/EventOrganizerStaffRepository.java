package ee.sda.training.eventmanager.repository;

import ee.sda.training.eventmanager.model.EventOrganizerStaff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface EventOrganizerStaffRepository extends JpaRepository<EventOrganizerStaff, Long>, JpaSpecificationExecutor<EventOrganizerStaff> {

}
