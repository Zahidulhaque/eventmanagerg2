package ee.sda.training.eventmanager.model;

import java.io.Serializable;
import java.util.Objects;

public class UserAuthorityId implements Serializable {
    private Long userId;
    private String authorityName;

    // default constructor
    public UserAuthorityId() {
    }

    public UserAuthorityId(Long userId, String authorityName) {
        this.userId = userId;
        this.authorityName = authorityName;
    }

    // equals() and hashCode()

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserAuthorityId)) return false;
        UserAuthorityId that = (UserAuthorityId) o;
        return userId.equals(that.userId) && authorityName.equals(that.authorityName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, authorityName);
    }
}

