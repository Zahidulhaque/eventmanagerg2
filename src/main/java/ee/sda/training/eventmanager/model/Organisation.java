package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.sql.Date;

/**
 * Data source of organisations
 */
@Data
@Entity
@Table(name = "organisation")
@ApiModel("Data source of organisations")
public class Organisation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "known_name", nullable = false)
    private String knownName;

    @Column(name = "known_abbreviation_name")
    private String knownAbbreviationName;

    @Column(name = "official_registry_name")
    private String officialRegistryName;

    @Column(name = "type")
    private String type;

    @Column(name = "registry_code")
    private String registryCode;

    @Column(name = "created")
    private LocalDate created;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "ended")
    private Date ended;

    @JsonIgnoreProperties(value = { "ownerOrg" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    PrimaryContactInfo primaryContact;

}
