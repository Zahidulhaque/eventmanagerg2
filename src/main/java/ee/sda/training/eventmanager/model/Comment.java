package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ee.sda.training.eventmanager.model.enumeration.Language;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Comment at an event
 */
@Data
@Entity
@Table(name = "comment")
@ApiModel("Comment at an event")
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "comment_text")
    private String commentText;

    @Enumerated(EnumType.STRING)
    @Column(name = "language")
    private Language language;

    @ManyToOne
    @JoinColumn(name = "event_id")
    @JsonIgnoreProperties(
        value = { "eventOrganizerAndContact", "eventTimeAndVenues", "abouts", "eventRegistrations", "subEvents", "isPartOfEvent" },
        allowSetters = true
    )
    private Event event;

}
