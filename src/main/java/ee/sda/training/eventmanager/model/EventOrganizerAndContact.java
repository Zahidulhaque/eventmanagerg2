package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Data about event organizers
 */
@Data
@Entity
@ApiModel("Data about event organizers ")
@Table(name = "event_organizer_and_contact")
public class EventOrganizerAndContact implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "organisation_id")
    private Organisation organisation;

    @OneToMany(mappedBy = "eventOrganizerAndContact")
    @JsonIgnoreProperties(value = { "eventOrganizerAndContact" }, allowSetters = true)
    private List<EventOrganizerStaff> eventOrganizerStaffs;

    @JsonIgnoreProperties(
        value = { "eventOrganizerAndContact", "eventTimeAndVenues", "abouts", "eventRegistrations", "subEvents", "isPartOfEvent" },
        allowSetters = true
    )
    @OneToOne(mappedBy = "eventOrganizerAndContact")
    private Event event;

    @ManyToOne
    @JoinColumn(name = "created_by")
    @JsonIgnoreProperties(value = {
            "firstName",
            "lastName",
            "resetDate",
            "activated",
            "langKey",
            "imageUrl"
    })
    User createdBy;
}
