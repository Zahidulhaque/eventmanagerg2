package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ee.sda.training.eventmanager.model.enumeration.PhoneType;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A PrimaryPhone.
 */
@Data
@Entity
@Table(name = "primary_phone")
public class PrimaryPhone implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "phone_number")
    private Integer phoneNumber;

    @Column(name = "formatted_number")
    private String formattedNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private PhoneType type;

    @ManyToOne
    @JsonIgnoreProperties(value = { "email", "primaryPhones", "address", "ownerPerson", "ownerOrg" }, allowSetters = true)
    private PrimaryContactInfo primaryContact;

}
