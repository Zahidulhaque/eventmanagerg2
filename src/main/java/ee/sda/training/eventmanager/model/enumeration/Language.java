package ee.sda.training.eventmanager.model.enumeration;

/**
 * The Language enumeration.
 */
public enum Language {
    et("et", "Estonian"),
    en("en", "English"),
    fr("fr", "Français");

    private final String value;
    private final String text;

    Language(String value, String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }
}
