package ee.sda.training.eventmanager.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ee.sda.training.eventmanager.model.enumeration.IsAbout;
import ee.sda.training.eventmanager.model.enumeration.Language;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Class for textual and multilingual descriptions
 */
@Data
@Entity
@Table(name = "about")
@ApiModel("Class for textual and multilingual descriptions")
public class About implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    @Lob
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "language")
    private Language language;

    @Enumerated(EnumType.STRING)
    @Column(name = "is_about")
    private IsAbout isAbout;

    @Column(name = "parent_id")
    private Integer parentId;

    // may I ask will it be possible "conditional relation" depending on the value of IsAbount (Event or Venue)
    @ManyToOne
/*
    @JoinColumns({
            @JoinColumn(name = "parent_id"),
            @JoinColumn(name = "is_about")
    })
*/
    @JoinColumn(name = "event_id")
    @JsonBackReference
    @JsonIgnoreProperties(
        value = { "eventOrganizerAndContact", "eventTimeAndVenues", "abouts", "eventRegistrations", "subEvents", "isPartOfEvent" },
        allowSetters = true
    )
    private Event event;

    @ManyToOne
/*
    @JoinColumns({
            @JoinColumn(name = "parent_id"),
            @JoinColumn(name = "is_about")
    })
*/
    @JoinColumn(name = "venue_id")
    @JsonBackReference
    @JsonIgnoreProperties(value = { "abouts", "address" }, allowSetters = true)
    private Venue venue;

}
