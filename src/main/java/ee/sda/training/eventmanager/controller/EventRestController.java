package ee.sda.training.eventmanager.controller;

import ee.sda.training.eventmanager.model.Event;
import ee.sda.training.eventmanager.model.EventTimeVenue;
import ee.sda.training.eventmanager.model.enumeration.Language;
import ee.sda.training.eventmanager.repository.EventRepository;
//import io.swagger.v3.oas.annotations.parameters.RequestBody;
import ee.sda.training.eventmanager.repository.EventTimeVenueRepository;
import ee.sda.training.eventmanager.service.EventService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

//import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/eventapi")
public class EventRestController {

    final private EventRepository eventRepository;
    final private EventService eventService;

    // Perhaps we need filtering by language also here
    @GetMapping("/events")
    List<Event> listByEvent(@RequestBody(required = false) Event event,
                                  @RequestParam(value = "language", required = false) String language,
                                  HttpServletResponse response) {

        response.addHeader("Access-Control-Allow-Origin","*");
        return eventRepository.findAll();
    }


    @GetMapping("/events/{id}/admin")
    Optional<Event> viewOne(@RequestBody(required = false) Event event,
                            @PathVariable("id") Long id,
                            HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin","*");
        return eventRepository.findById(id);
    }

    @GetMapping("/events/{id}/public")
    Optional<Event> publicViewOne(@RequestBody(required = false) Event event,
                                  @PathVariable("id") Long id,
                                  @RequestParam(value = "language", required = false) String language,
                                   HttpServletResponse response) {

        response.addHeader("Access-Control-Allow-Origin","*");
        return eventService.findByIdAndLanguage(id, language);
    }

    @GetMapping("/events/whenwhere")
    List<EventTimeVenue> listByOccurrence(@RequestBody(required = false) EventTimeVenue eventTimeVenue,
                                          @RequestParam(value = "language", required = false) String language,
                                                  HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin","*");
        return eventService.findAllUpcomingByLanguage(language, LocalDate.now());
    }
}
