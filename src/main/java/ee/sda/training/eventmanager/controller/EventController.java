package ee.sda.training.eventmanager.controller;

import ee.sda.training.eventmanager.model.EventTimeVenue;
import ee.sda.training.eventmanager.repository.EventRepository;
import ee.sda.training.eventmanager.service.EventService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("")
public class EventController {
    //Get event list

    final private EventService eventService;
    final private EventRepository eventRepository;

    @GetMapping({"", "/", "index.html"})
    ModelAndView getAllEventsByOccurrence(
            @RequestParam(value = "language", required = false) String language,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate localDate
    ) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("title", "Events: ");
        localDate = LocalDate.now();
        if (language == null) language = "et";
        modelAndView.addObject("occurrences", eventService.findAllUpcomingByLanguage(language, localDate));
        return modelAndView;
    }


    @GetMapping("/adminevent/{id}")
    ModelAndView eventAdminDashboard(@PathVariable("id") Long id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("adminevent");
        if (eventRepository.findById(id).isPresent())
        modelAndView.addObject("event", eventRepository.findById(id).get());
        return modelAndView;
    }

}
